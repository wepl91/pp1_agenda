CREATE DATABASE `grupo_9`;
USE grupo_9;

CREATE TABLE `etiquetas`
(
    `id` int NOT NULL AUTO_INCREMENT,
    `Nombre` varchar(45) NOT NULL,
    PRIMARY KEY(`id`)
);

CREATE TABLE `localidades`
(
    `id` int NOT NULL AUTO_INCREMENT,
    `Nombre` varchar(45) NOT NULL,

    PRIMARY KEY(`id`)
);

CREATE TABLE `personas`
(
  `id` int NOT NULL AUTO_INCREMENT,
  `Nombre` varchar(45) NOT NULL,
  `Telefono` varchar(20) NOT NULL,
  `Email` varchar(30),
  `Nacimiento` date,
  `calle`    varchar(100),
  `altura` 	 int,
  `piso` 	 int,
  `dept`     VARCHAR(20),
  `idEtiqueta` int,
  `idLocalidad` int,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`idEtiqueta`) REFERENCES etiquetas(id),
  FOREIGN KEY (`idLocalidad`) REFERENCES localidades(id)
);

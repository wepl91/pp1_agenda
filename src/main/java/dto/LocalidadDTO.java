package dto;

public class LocalidadDTO {
	private int id;
	private String nombre;
	
	public LocalidadDTO(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }
	
    public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String toString() {
        return nombre;
    }
	
	@Override
    public boolean equals(Object obj) {
        if(obj==null){
            return false;
        }

        if(obj instanceof LocalidadDTO){
            return ((LocalidadDTO)obj).getId() == this.id;
        }
        else {
            return super.equals(obj);
        }
    }
}

package dto;

import java.util.Date;

public class PersonaDTO {
	
	private int id;
	private String nombre;
	private String telefono;
	private String email;
	private Date nacimiento;
	private String calle;
	private Integer altura;
	private Integer piso;
	private String dept;
	private int idEtiqueta;
	private int idLocalidad;

	public PersonaDTO(int id, String nombre, String telefono, String email, Date nacimiento) {
		this.id = id;
		this.nombre = nombre;
		this.telefono = telefono;
		this.email = email;
		this.nacimiento = nacimiento;
	}

	public String getEmail() {
		return email;
	}

	public Date getNacimiento() {
		return nacimiento;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setNacimiento(Date nacimiento) {
		this.nacimiento = nacimiento;
	}


	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getTelefono() {
		return this.telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public Integer getAltura() {
		return altura;
	}

	public void setAltura(Integer altura) {
		this.altura = altura;
	}

	public Integer getPiso() {
		return piso;
	}

	public void setPiso(Integer piso) {
		this.piso = piso;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	public int getIdLocalidad() {
		return idLocalidad;
	}

	public void setIdLocalidad(int idLocalidad) {
		this.idLocalidad = idLocalidad;
	}

	public int getIdEtiqueta() {
		return idEtiqueta;
	}

	public void setIdEtiqueta(int idEtiqueta) {
		this.idEtiqueta = idEtiqueta;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
}

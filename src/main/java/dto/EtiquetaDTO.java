package dto;

public class EtiquetaDTO {
    private int id;
    private String nombre;

    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public EtiquetaDTO(int id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }
    public String toString() {
        return nombre;
    }
    
    @Override
    public boolean equals(Object obj) {
        if(obj==null){
            return false;
        }

        if(obj instanceof EtiquetaDTO){
            return ((EtiquetaDTO)obj).getId() == this.id;
        }
        else {
            return super.equals(obj);
        }
    }
}

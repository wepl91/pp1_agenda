package dto;

import java.util.Calendar;
import java.util.Date;

public class PersonaReportDTO {
	private String nombre;
	private String telefono;
	private String email;
	private Date nacimiento;
	private String calle;
	private Integer altura;
	private Integer piso;
	private String dept;
	private String localidad;
	private String etiqueta;
	private String signo;
	
	private int[] zodiacosLastDay= {20,19,20,20,21,21,23,23,23,23,23,21};
	private String[] zodiaco= {"Capricornio","Acuario","Piscis","Aries","Tauro","Géminis","Cáncer","Leo","Virgo","Libra","Escorpio","Sagitario"};
	
	
	public PersonaReportDTO(PersonaDTO persona) {
		this.nombre=persona.getNombre();
		this.telefono=persona.getTelefono();
		this.email=persona.getEmail();
		this.nacimiento=persona.getNacimiento();
		this.calle=persona.getCalle();
		this.piso=persona.getPiso();
		this.altura=persona.getAltura();
		this.dept=persona.getDept();
				
		this.signo=calcularSigno(this.nacimiento);
		
	}
	
	private String calcularSigno(Date diaNacimiento) {
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(diaNacimiento);
		
		int month=calendar.get(Calendar.MONTH);
		int day=calendar.get(Calendar.DAY_OF_MONTH);
		
		if(day>zodiacosLastDay[month]) month++;
		return zodiaco[month];
	}
	
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getNacimiento() {
		return nacimiento;
	}
	public void setNacimiento(Date nacimiento) {
		this.nacimiento = nacimiento;
	}
	public String getEtiqueta() {
		return etiqueta;
	}
	public void setEtiqueta(String etiqueta) {
		this.etiqueta = etiqueta;
	}

	public String getSigno() {
		return signo;
	}

	public void setSigno(String signo) {
		this.signo = signo;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public Integer getAltura() {
		return altura;
	}

	public void setAltura(Integer altura) {
		this.altura = altura;
	}

	public Integer getPiso() {
		return piso;
	}

	public void setPiso(Integer piso) {
		this.piso = piso;
	}

	public String getDept() {
		return dept;
	}

	public void setDept(String dept) {
		this.dept = dept;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}
	
}

package modelo;

import java.util.List;

import dto.EtiquetaDTO;
import dto.LocalidadDTO;
import dto.PersonaDTO;
import persistencia.dao.interfaz.DAOAbstractFactory;
import persistencia.dao.interfaz.EtiquetaDAO;
import persistencia.dao.interfaz.LocalidadDAO;
import persistencia.dao.interfaz.PersonaDAO;


public class Agenda {
	private PersonaDAO persona;
	private EtiquetaDAO etiqueta;
	private LocalidadDAO localidad;
	
	public Agenda(DAOAbstractFactory metodo_persistencia) {
		this.persona = metodo_persistencia.createPersonaDAO();
		this.etiqueta = metodo_persistencia.createEtiquetaDAO();
		this.localidad = metodo_persistencia.createLocalidadDAO();
	}
	
	public void agregarPersona(PersonaDTO nuevaPersona) {
		this.persona.insert(nuevaPersona);
	}

	public PersonaDTO buscarPersona(int personaId) {
		return this.persona.find(personaId);
	}

	public void actualizarPersona(PersonaDTO persona) {
		this.persona.update(persona);
	}

	public void borrarPersona(PersonaDTO persona_a_eliminar)  {
		this.persona.delete(persona_a_eliminar);
	}
	
	public List<PersonaDTO> obtenerPersonas() {
		return this.persona.readAll();		
	}

	public List<EtiquetaDTO> obtenerEtiquetas() {
		return this.etiqueta.readAll();
	}

	public boolean agregarEtiqueta(EtiquetaDTO nuevaEtiqueta) {
		return this.etiqueta.insert(nuevaEtiqueta);
	}

	public boolean borrarEtiqueta(EtiquetaDTO etiqueta) {
		return this.etiqueta.delete(etiqueta);
	}

	public boolean actualizarEtiqueta(EtiquetaDTO etiqueta) {
		return this.etiqueta.update(etiqueta);
	}

	public EtiquetaDTO buscarEtiqueta(int etiquetaId) {
		return this.etiqueta.find(etiquetaId);
	}
	
	public List<LocalidadDTO> obtenerLocalidades() {
		return this.localidad.readAll();
	}
	
	public boolean agregarLocalidad(LocalidadDTO localidad) {
		return this.localidad.insert(localidad);
	}
	
	public LocalidadDTO buscarLocalidad(int id) {
		return this.localidad.find(id);
	}
	
	public boolean actualizarLocalidad(LocalidadDTO localidad) {
		return this.localidad.update(localidad);
	}
	
	public boolean borrarLocalidad(LocalidadDTO localidad) {
		return this.localidad.delete(localidad);
	}
}

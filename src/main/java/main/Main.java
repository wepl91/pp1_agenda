package main;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;

import modelo.Agenda;
import persistencia.conexion.Conexion;
import persistencia.dao.mysql.DAOSQLFactory;
import presentacion.controlador.ControladorConexion;
import presentacion.controlador.ControladorEtiqueta;
import presentacion.controlador.ControladorLocalidad;
import presentacion.controlador.ControladorPersona;
import presentacion.vista.VistaPersona;
import java.util.Properties;

public class Main {

	/**
	 * Metodo principal y inicio de ejecucion
	 */
	public static void main(String[] args) {

		VistaPersona vistaPersona = new VistaPersona();
		Agenda agenda = new Agenda(new DAOSQLFactory());
		
		ControladorPersona controladorPersona = new ControladorPersona(vistaPersona, agenda);
		ControladorEtiqueta controladorEtiqueta = new ControladorEtiqueta(vistaPersona, agenda);
		ControladorLocalidad controladorLocalidad = new ControladorLocalidad(vistaPersona, agenda);
		
		ControladorConexion controladorConexion = new ControladorConexion(vistaPersona,
				controladorPersona,controladorEtiqueta,controladorLocalidad);
		
		if(controladorConexion.testConectionFromProperties()) {
			controladorConexion.inicializarApp();
		} else {
			controladorConexion.showVentanaDb();
		}
	}

}

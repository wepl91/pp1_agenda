package presentacion.vista;

import dto.EtiquetaDTO;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.util.List;

public class VistaEtiqueta extends JFrame {
    private JFrame frame;
    private JTable tablaEtiquetas;
    private JButton btnAgregarEtiqueta;
    private JButton btnEditarEtiqueta;
    private JButton btnBorrarEtiqueta;
    private JButton btnSalir;
    private DefaultTableModel modelEtiquetas;
    private static VistaEtiqueta INSTANCE;
    private  String[] nombreColumnas = {"id","Nombre"};

    public VistaEtiqueta() {
        super();
        initialize();
    }

    public static VistaEtiqueta getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new VistaEtiqueta();
            return new VistaEtiqueta();
        }
        else {
            return INSTANCE;
        }
    }

    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 410, 300);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        JPanel panel = new JPanel();
        panel.setBounds(0, 0, 410, 290);
        frame.getContentPane().add(panel);
        panel.setLayout(null);

        modelEtiquetas = new DefaultTableModel(null,nombreColumnas);
        tablaEtiquetas = new JTable(modelEtiquetas);

        JScrollPane spEtiquetas = new JScrollPane();
        spEtiquetas.setBounds(10, 10, 390, 190);
        panel.add(spEtiquetas);

        tablaEtiquetas.getColumnModel().getColumn(0).setPreferredWidth(103);
        tablaEtiquetas.getColumnModel().getColumn(0).setResizable(false);
        tablaEtiquetas.getColumnModel().getColumn(1).setPreferredWidth(100);
        tablaEtiquetas.getColumnModel().getColumn(1).setResizable(false);

        spEtiquetas.setViewportView(tablaEtiquetas);

        btnAgregarEtiqueta = new JButton("Agregar");
        btnAgregarEtiqueta.setBounds(10, 240, 89, 23);
        panel.add(btnAgregarEtiqueta);

        btnEditarEtiqueta = new JButton("Editar");
        btnEditarEtiqueta.setBounds(110, 240, 89, 23);
        panel.add(btnEditarEtiqueta);

        btnBorrarEtiqueta = new JButton("Borrar");
        btnBorrarEtiqueta.setBounds(210, 240, 89, 23);
        panel.add(btnBorrarEtiqueta);

        btnSalir = new JButton("Salir");
        btnSalir.setBounds(310, 240, 89, 23);
        panel.add(btnSalir);

    }

    public JButton getBtnSalir() {
    	return btnSalir;
    }
    
    public JFrame getFrame() {
    	return frame;
    }

    public JButton getBtnAgregarEtiqueta() {
        return btnAgregarEtiqueta;
    }

    public JButton getBtnEditarEtiqueta() {
        return btnEditarEtiqueta;
    }

    public JButton getBtnBorrarEtiqueta() {
        return btnBorrarEtiqueta;
    }

    public boolean confirmarBorrado() {
        return(JOptionPane.showOptionDialog(null,
                "¿Estás seguro que quieres borrar las etiquetas selecionadas?",
                "Confirmación",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                null,
                null) == 0);
    }

    public void mostrarAdvertencia(String advertencia) {
        JOptionPane.showMessageDialog(null, advertencia);
    }

    public void show() {
        this.frame.setVisible(true);
    }

    public String[] getNombreColumnas() {
        return nombreColumnas;
    }

    public DefaultTableModel getModelEtiquetas() {
        return modelEtiquetas;
    }

    public JTable getTablaEtiquetas() {
        return tablaEtiquetas;
    }

    public void llenarTabla(List<EtiquetaDTO> personasEnTabla) {
        this.getModelEtiquetas().setRowCount(0); //Para vaciar la tabla
        this.getModelEtiquetas().setColumnCount(0);
        this.getModelEtiquetas().setColumnIdentifiers(this.getNombreColumnas());

        for (EtiquetaDTO p : personasEnTabla) {
            String nombre = p.getNombre();
            int id = p.getId();
            Object[] fila = {id,nombre};
            this.getModelEtiquetas().addRow(fila);
        }

    }
}

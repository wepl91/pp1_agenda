package presentacion.vista;

import dto.EtiquetaDTO;
import dto.LocalidadDTO;
import dto.PersonaDTO;
import org.jdatepicker.UtilDateModel;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.*;
import java.util.List;

public class VentanaPersona extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField txtId;
	private JTextField txtNombre;
	private JTextField txtEmail;
	private JSpinner nacimiento;
	private JCheckBox checkNacimiento;
	private JTextField txtEtiqueta;
	private JTextField txtTelefono;
	private JButton btnAgregarPersona;
	private JButton btnGuardarPersona;
	private JButton btnSalir;
	private static VentanaPersona INSTANCE;
	private JTextField txtCalle;
	private JTextField txtAltura;
	private JTextField txtDepartamento;
	private JTextField txtPiso;
	private JComboBox<EtiquetaDTO> listEtiquetas;
	private JComboBox<LocalidadDTO> listLocalidad;
	
	public static VentanaPersona getInstance() {
		if(INSTANCE == null) {
			INSTANCE = new VentanaPersona(); 	
			return new VentanaPersona();
		}
		else {
			return INSTANCE;
		}
	}

	private VentanaPersona() {
		super();
		UtilDateModel model = new UtilDateModel();
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 450, 450);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(5, 5, 450, 440);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblDatos = new JLabel("<html><u>Datos personales</u></html>");
		lblDatos.setBounds(10, 15, 130, 16);
		panel.add(lblDatos);

		JLabel lblNombreYApellido = new JLabel("Nombre y apellido");
		lblNombreYApellido.setBounds(10, 40, 130, 14);
		panel.add(lblNombreYApellido);

		JLabel lblTelfono = new JLabel("Telefono");
		lblTelfono.setBounds(10, 65, 130, 14);
		panel.add(lblTelfono);

		JLabel lblEmail = new JLabel("Email");
		lblEmail.setBounds(10, 90, 130, 14);
		panel.add(lblEmail);

		JLabel lblNacimiento = new JLabel("Nacimiento");
		lblNacimiento.setBounds(10, 115, 110, 14);
		panel.add(lblNacimiento);

		checkNacimiento = new JCheckBox();
		checkNacimiento.setBounds(375, 115, 20, 20);
		checkNacimiento.setSelected(false);
		checkNacimiento.addChangeListener(this::checkToggle);
		panel.add(checkNacimiento);

		JLabel lblEtiqueta = new JLabel("Etiqueta");
		lblEtiqueta.setBounds(10, 140, 130, 14);
		panel.add(lblEtiqueta);

		JLabel lblDomicilio = new JLabel("<html><u>Domicilio</u></html>");
		lblDomicilio.setBounds(10, 180, 130, 16);
		panel.add(lblDomicilio);


		JLabel lblCalle = new JLabel("Calle");
		lblCalle.setBounds(10, 205, 61, 16);
		panel.add(lblCalle);

		JLabel lblAltura = new JLabel("Altura");
		lblAltura.setBounds(10, 230, 61, 16);
		panel.add(lblAltura);

		JLabel lblPiso = new JLabel("Piso");
		lblPiso.setBounds(10, 255, 61, 16);
		panel.add(lblPiso);

		JLabel lblDept = new JLabel("Dept");
		lblDept.setBounds(10, 280, 61, 16);
		panel.add(lblDept);

		JLabel lblLocalidad = new JLabel("Localidad");
		lblLocalidad.setBounds(10, 305, 61, 16);
		panel.add(lblLocalidad);

		txtNombre = new JTextField();
		txtNombre.setBounds(200, 40, 200, 20);
		txtNombre.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if(txtNombre.getText().length() >= 45) {
					JOptionPane.showMessageDialog(null, "El nombre del contacto no puede superar los 45 caracteres.");
					e.consume();
				}
			}
		});
		panel.add(txtNombre);
		txtNombre.setColumns(10);

		txtTelefono = new JTextField();
		txtTelefono.setBounds(200, 65, 200, 20);
		txtTelefono.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if(txtTelefono.getText().length() >= 20) {
					JOptionPane.showMessageDialog(null, "El teléfono del contacto no puede superar los 20 caracteres.");
					e.consume();
				}
			}
		});
		panel.add(txtTelefono);
		txtTelefono.setColumns(10);

		txtEmail = new JTextField();
		txtEmail.setBounds(200, 90, 200, 20);
		txtEmail.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if(txtEmail.getText().length() >= 30) {
					JOptionPane.showMessageDialog(null, "El email del contacto no puede superar los 30 caracteres.");
					e.consume();
				}
			}
		});
		panel.add(txtEmail);
		txtEmail.setColumns(10);

		SpinnerModel dateModel = new SpinnerDateModel();
		nacimiento = new JSpinner(dateModel);
		JSpinner.DateEditor dateEditor = new JSpinner.DateEditor(nacimiento, "yyyy-MM-dd");
		nacimiento.setEditor(dateEditor);
		nacimiento.setBounds(200,115,170,20);
		panel.add(nacimiento);

		listEtiquetas = new JComboBox<>();
		listEtiquetas.setBounds(200,140,200,20);
		panel.add(listEtiquetas);


		txtCalle = new JTextField();
		txtCalle.setBounds(200, 205, 200, 20);
		txtCalle.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if(txtCalle.getText().length() >= 100) {
					JOptionPane.showMessageDialog(null, "La calle del contacto no puede superar los 30 caracteres.");
					e.consume();
				}
			}
		});
		panel.add(txtCalle);
		txtCalle.setColumns(10);
		
		txtAltura = new JTextField();
		txtAltura.setBounds(200, 230, 200, 20);
		panel.add(txtAltura);
		txtAltura.setColumns(10);

		txtPiso = new JTextField();
		txtPiso.setColumns(10);
		txtPiso.setBounds(200, 255, 200, 20);
		panel.add(txtPiso);
		
		txtDepartamento = new JTextField();
		txtDepartamento.setBounds(200, 280, 200, 20);
		txtDepartamento.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if(txtDepartamento.getText().length() >= 20) {
					JOptionPane.showMessageDialog(null, "El departamento del contacto no puede superar los 20 caracteres.");
					e.consume();
				}
			}
		});
		panel.add(txtDepartamento);
		txtDepartamento.setColumns(10);

		listLocalidad = new JComboBox<LocalidadDTO>();
		listLocalidad.setBounds(200, 305, 200, 20);
		panel.add(listLocalidad);

		txtId = new JTextField();
		panel.add(txtId);
		txtId.setVisible(false);
		
		btnAgregarPersona = new JButton("Agregar");
		btnAgregarPersona.setBounds(265, 390, 90, 25);
		panel.add(btnAgregarPersona);

		btnGuardarPersona = new JButton("Guardar");
		btnGuardarPersona.setBounds(265, 390, 90, 25);
		panel.add(btnGuardarPersona);

		btnSalir = new JButton("Salir");
		btnSalir.setBounds(347, 390, 90, 25);
		btnSalir.addActionListener(this::cerrarVentana);
		panel.add(btnSalir);
		this.setVisible(false);
	}

	private void cerrarVentana(ActionEvent actionEvent) {
		this.cerrar();
	}

	public void crearPersona() {
		if (listEtiquetas.getItemCount() > 0) {
			listEtiquetas.setSelectedIndex(0);
		}
		else {
			listEtiquetas.setEnabled(false);
		}

		if (listLocalidad.getItemCount() > 0) {
			listLocalidad.setSelectedIndex(0);
		}
		else {
			listLocalidad.setEnabled(false);
		}

		if (checkNacimiento.isSelected()) {
			nacimiento.setValue(new Date());
		}
		else {
			nacimiento.setEnabled(false);
		}
		nacimiento.setEditor(new JSpinner.DateEditor(nacimiento,"yyyy-MM-dd"));
		txtNombre.setText(null);
		txtTelefono.setText(null);
		txtEmail.setText(null);
		txtCalle.setText(null);
		txtAltura.setText(null);
		txtPiso.setText(null);
		txtDepartamento.setText(null);
		btnGuardarPersona.setVisible(false);
		btnAgregarPersona.setVisible(true);
		this.setVisible(true);
	}

	public void editarPersona(PersonaDTO persona, EtiquetaDTO etiqueta, LocalidadDTO localidad) {
		nacimiento.setValue(persona.getNacimiento() != null ? persona.getNacimiento() : new Date());
		nacimiento.setEditor(new JSpinner.DateEditor(nacimiento,"yyyy-MM-dd"));
		nacimiento.setEnabled(persona.getNacimiento() != null);
		checkNacimiento.setSelected(persona.getNacimiento() != null);
		txtId.setText(String.valueOf(persona.getId()));
		txtNombre.setText(persona.getNombre());
		txtTelefono.setText(persona.getTelefono());
		txtEmail.setText(persona.getEmail());
		txtCalle.setText(persona.getCalle());
		txtAltura.setText(persona.getAltura().toString());
		txtPiso.setText(persona.getPiso().toString());
		txtDepartamento.setText(persona.getDept());

		listEtiquetas.setSelectedItem(etiqueta);
		listLocalidad.setSelectedItem(localidad);
		
		btnGuardarPersona.setVisible(true);
		btnAgregarPersona.setVisible(false);
		this.setVisible(true);
	}

	public void checkToggle(ChangeEvent e) {
		this.nacimiento.setEnabled(this.checkNacimiento.isSelected());
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

    public JTextField getTxtTelefono() {
        return txtTelefono;
    }

    public JTextField getTxtId() {
        return txtId;
    }

    public JTextField getTxtEmail() {
		return txtEmail;
	}

	public JSpinner getNacimiento() {
		return nacimiento;
	}

	public JTextField getTxtEtiqueta() {
		return txtEtiqueta;
	}

	public JButton getBtnAgregarPersona() {
		return btnAgregarPersona;
	}

	public JButton getBtnEditarPersona() {
		return btnGuardarPersona;
	}

	public JComboBox<EtiquetaDTO> getListEtiquetas() {
		return listEtiquetas;
	}

	public void llenarComboEtiqueta(List<EtiquetaDTO> etiquetas) {
		this.listEtiquetas.removeAllItems();
		for (EtiquetaDTO etiqueta : etiquetas) {
			this.listEtiquetas.addItem(etiqueta);
		}
	}
	
	public void llenarComboLocalidad(List<LocalidadDTO> localidades) {
		listLocalidad.removeAllItems();
		for (LocalidadDTO localidad : localidades) {
			listLocalidad.addItem(localidad);
		}
	}

	public void cerrar() {
		this.txtNombre.setText(null);
		this.txtTelefono.setText(null);
		this.txtId.setText(null);
		this.txtEmail.setText(null);
		this.nacimiento.setValue(new Date());
		this.checkNacimiento.setSelected(true);
		this.txtCalle.setText(null);
		this.txtAltura.setText(null);
		this.txtDepartamento.setText(null);
		this.txtPiso.setText(null);
		this.listLocalidad.setSelectedItem(null);
		this.listEtiquetas.setSelectedItem(null);
		this.dispose();
	}

	public JTextField getTxtCalle() {
		return txtCalle;
	}

	public JTextField getTxtAltura() {
		return txtAltura;
	}

	public JTextField getTxtDepartamento() {
		return txtDepartamento;
	}

	public JTextField getTxtPiso() {
		return txtPiso;
	}

	public JComboBox<LocalidadDTO> getListLocalidad() {
		return listLocalidad;
	}
}


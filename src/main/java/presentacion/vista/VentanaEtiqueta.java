package presentacion.vista;

import dto.EtiquetaDTO;
import dto.PersonaDTO;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class VentanaEtiqueta extends JFrame {
    private static final long serialVersionUID = 1L;
    private JPanel contentPane;
    private JButton btnAgregar;
    private JButton btnEditar;
    private JButton btnSalir;
    private JTextField txtNombre;
    private JTextField txtId;
    private static VentanaEtiqueta INSTANCE;

    public static VentanaEtiqueta getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new VentanaEtiqueta();
            return new VentanaEtiqueta();
        }
        else {
            return INSTANCE;
        }
    }

    private VentanaEtiqueta() {
        super();

        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setBounds(100, 100, 400, 250);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        JPanel panel = new JPanel();
        panel.setBounds(10, 11, 440, 307);
        contentPane.add(panel);
        panel.setLayout(null);

        JLabel lblNombreYApellido = new JLabel("Nombre de etiqueta");
        lblNombreYApellido.setBounds(10, 11, 130, 14);
        panel.add(lblNombreYApellido);

        txtNombre = new JTextField();
        txtNombre.setBounds(200, 8, 160, 20);
        txtNombre.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if(txtNombre.getText().length() >= 45) {
                    JOptionPane.showMessageDialog(null, "El nombre de la etiqueta no puede superar los 45 caracteres.");
                    e.consume();
                }
            }
        });
        panel.add(txtNombre);
        txtNombre.setColumns(10);

        btnAgregar = new JButton("Agregar");
        btnAgregar.setBounds(190, 185, 89, 23);
        panel.add(btnAgregar);

        btnEditar = new JButton("Guardar");
        btnEditar.setBounds(190, 185, 89, 23);
        panel.add(btnEditar);

        btnSalir = new JButton("Salir");
        btnSalir.setBounds(290, 185, 89, 23);
        btnSalir.addActionListener(this::cerrarVentana);
        panel.add(btnSalir);


        txtId = new JTextField();
        panel.add(txtId);
        txtId.setVisible(false);

    }

    private void cerrarVentana(ActionEvent actionEvent) {
        this.dispose();
    }

    public void crearEtiqueta() {
        btnEditar.setVisible(false);
        btnAgregar.setVisible(true);
        txtNombre.setText(null);
        this.setVisible(true);
    }

    public void editarEtiqueta(EtiquetaDTO etiquetaDTO) {
        btnAgregar.setVisible(false);
        btnEditar.setVisible(true);
        txtNombre.setText(etiquetaDTO.getNombre());
        txtId.setText(String.valueOf(etiquetaDTO.getId()));
        this.setVisible(true);
    }

    public JButton getBtnAgregar() {
        return btnAgregar;
    }

    public JButton getBtnEditar() {
        return btnEditar;
    }

    public JTextField getTxtNombre() {
        return txtNombre;
    }

    public JTextField getTxtId() {
        return txtId;
    }

    public void cerrar()  {
        this.txtNombre.setText(null);
        this.dispose();
    }

    public void showVentanaEtiqueta() {
        this.setVisible(true);
    }
}

package presentacion.vista;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Date;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import dto.EtiquetaDTO;
import dto.LocalidadDTO;
import dto.PersonaDTO;

import javax.swing.JButton;

import persistencia.conexion.Conexion;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VistaPersona {
	private JFrame frame;
	private JTable tablaPersonas;
	private JButton btnAgregar;
	private JButton btnEditar;
	private JButton btnBorrar;
	private JButton btnReporte;
	private JButton btnSalir;
	private JButton btnEditarConexion;
	private JButton btnLocalidades;
	private JButton btnEtiquetas;
	private JFrame frmAdvertencia;
	private DefaultTableModel modelPersonas;
	private  String[] nombreColumnas = {"Nombre y apellido","Telefono,", "Email", "Nacimiento", "Dirección", "Etiqueta"};

	public VistaPersona() {
		super();
		initialize();
	}


	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 1200, 400);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 1190, 390);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JScrollPane spPersonas = new JScrollPane();
		spPersonas.setBounds(10, 11, 960, 260);
		panel.add(spPersonas);
		
		modelPersonas = new DefaultTableModel(null,nombreColumnas);
		tablaPersonas = new JTable(modelPersonas);
		
		tablaPersonas.getColumnModel().getColumn(0).setPreferredWidth(103);
		tablaPersonas.getColumnModel().getColumn(0).setResizable(false);
		tablaPersonas.getColumnModel().getColumn(1).setPreferredWidth(100);
		tablaPersonas.getColumnModel().getColumn(1).setResizable(false);
		
		spPersonas.setViewportView(tablaPersonas);

		btnEditarConexion = new JButton("Editar conexión");
		btnEditarConexion.setBounds(5, 340, 150, 23);
		panel.add(btnEditarConexion);

		btnLocalidades = new JButton("Localidades");
		btnLocalidades.setBounds(995, 11, 180, 23);
		panel.add(btnLocalidades);

		btnEtiquetas = new JButton("Etiquetas");
		btnEtiquetas.setBounds(995, 40, 180, 23);
		panel.add(btnEtiquetas);

		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(492, 340, 89, 23);
		panel.add(btnAgregar);
		
		btnEditar = new JButton("Editar");
		btnEditar.setBounds(591, 340, 89, 23);
		panel.add(btnEditar);
		
		btnBorrar = new JButton("Borrar");
		btnBorrar.setBounds(690, 340, 89, 23);
		panel.add(btnBorrar);
		
		btnReporte = new JButton("Reporte");
		btnReporte.setBounds(789, 340, 89, 23);
		panel.add(btnReporte);

		btnSalir = new JButton("Salir");
		btnSalir.setBounds(888, 340, 89, 23);
		btnSalir.addActionListener(this::cerrarVista);
		panel.add(btnSalir);
	}

	private void cerrarVista(ActionEvent actionEvent) {
		int confirm = JOptionPane.showOptionDialog(
				null, "¿Estás seguro que quieres salir de la Agenda?",
				"Confirmación", JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE, null, null, null);
		if (confirm == 0) {
			this.frame.dispose();
		}
	}

	public void show() {
		this.frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.frame.addWindowListener(new WindowAdapter() {
		    public void windowClosing(WindowEvent e) {
				int confirm = JOptionPane.showOptionDialog(
		             null, "¿Estás seguro que quieres salir de la Agenda?", 
		             "Confirmación", JOptionPane.YES_NO_OPTION,
		             JOptionPane.QUESTION_MESSAGE, null, null, null);
		        if (confirm == 0) {
		        	Conexion.getConexion().cerrarConexion();
		           System.exit(0);
		        }
		    }
		});
		this.frame.setVisible(true);
	}

	public void mostrarAdvertencia(String advertencia) {
		JOptionPane.showMessageDialog(null, advertencia);
	}

	public boolean confirmarBorrado(String entidad) {
		return(JOptionPane.showOptionDialog(null,
				"¿Estás seguro que quieres borrar las "+ entidad + " selecionadas?",
				"Confirmación",
				JOptionPane.YES_NO_OPTION,
				JOptionPane.QUESTION_MESSAGE,
				null,
				null,
				null) == 0);
	}
	
	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public JButton getBtnLocalidades() {
		return btnLocalidades;
	}

	public JButton getBtnEtiquetas() {
		return btnEtiquetas;
	}

	public JButton getBtnBorrar() {
		return btnBorrar;
	}

	public JButton getBtnEditar() {
		return btnEditar;
	}

	public JButton getBtnReporte() {
		return btnReporte;
	}

	public JButton getBtnEditarConexion() {
		return btnEditarConexion;
	}

	public DefaultTableModel getModelPersonas() {
		return modelPersonas;
	}
	
	public JTable getTablaPersonas() {
		return tablaPersonas;
	}

	public String[] getNombreColumnas() {
		return nombreColumnas;
	}
	
	private LocalidadDTO encontrarLocalidad(PersonaDTO persona, List<LocalidadDTO> localidades) {
		for(LocalidadDTO localidad : localidades) {
			if(localidad.getId() == persona.getIdLocalidad()) {
				return localidad;
			}
		}
		return null;
	}
	
	private EtiquetaDTO encontrarEtiqueta(PersonaDTO persona, List<EtiquetaDTO> etiquetas) {
		for(EtiquetaDTO etiqueta : etiquetas) {
			if(etiqueta.getId() == persona.getIdEtiqueta()) {
				return etiqueta;
			}
		}
		return null;
	}


	public void llenarTabla(List<PersonaDTO> personasEnTabla, List<EtiquetaDTO> etiquetas, List<LocalidadDTO> localidades) {
		this.getModelPersonas().setRowCount(0); //Para vaciar la tabla
		this.getModelPersonas().setColumnCount(0);
		this.getModelPersonas().setColumnIdentifiers(this.getNombreColumnas());

		for (PersonaDTO p : personasEnTabla) {
			String nombre = p.getNombre();
			String tel = p.getTelefono();
			String email = p.getEmail();
			Date nacimiento = p.getNacimiento();
			String etiqueta = this.encontrarEtiqueta(p, etiquetas).getNombre();
			String direccion = p.getCalle() + " " + p.getAltura() + ", " + this.encontrarLocalidad(p, localidades).getNombre();
			Object[] fila = {nombre, tel, email, nacimiento, direccion, etiqueta};
			this.getModelPersonas().addRow(fila);
		}
		
	}
}

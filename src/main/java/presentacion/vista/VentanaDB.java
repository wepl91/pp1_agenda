package presentacion.vista;

import persistencia.conexion.Conexion;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VentanaDB {
    private JFrame frame;
    private JTextField txtDbIp;
    private JTextField txtDbUser;
    private JTextField txtPort;
    private JPasswordField txtDbPassword;
    private JLabel header;
    private JLabel lblIp;
    private JLabel lblUser;
    private JLabel lblPassword;
    private JLabel lblPort;
    private JButton btnConectar;
    private JButton btnGuardar;
    private JLabel errorLabel;

    private static VentanaDB INSTANCE;

    public VentanaDB() {
        super();
        initialize();
    }

    public static VentanaDB getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new VentanaDB();
        }
        return INSTANCE;
    }

    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 350, 250);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        JPanel panel = new JPanel();
        panel.setBounds(0, 0, 340, 240);
        frame.getContentPane().add(panel);
        panel.setLayout(null);

        header = new JLabel("<html><u>Configuración de la conexión a la base de datos</u></html>");
        header.setBounds(10, 10, 500, 14);
        panel.add(header);

        lblIp = new JLabel("IP:");
        lblIp.setBounds(10, 55, 100, 14);
        panel.add(lblIp);

        lblPort = new JLabel("Puerto:");
        lblPort.setBounds(10,80, 100, 14);
        panel.add(lblPort);

        lblUser = new JLabel("Usuario:");
        lblUser.setBounds(10, 105, 100, 14);
        panel.add(lblUser);

        lblPassword = new JLabel("Contraseña:");
        lblPassword.setBounds(10, 130, 100, 14);
        panel.add(lblPassword);

        txtDbIp = new JTextField();
        txtDbIp.setBounds(120, 55, 200, 20);
        panel.add(txtDbIp);

        txtPort = new JTextField();
        txtPort.setBounds(120,80,200,20);
        panel.add(txtPort);

        txtDbUser = new JTextField();
        txtDbUser.setBounds(120, 105, 200, 20);
        panel.add(txtDbUser);

        txtDbPassword = new JPasswordField();
        txtDbPassword.setBounds(120, 130, 200, 20);
        panel.add(txtDbPassword);

        btnGuardar = new JButton("Guardar");
        btnGuardar.setBounds(231, 190, 89, 23);
        panel.add(btnGuardar);

        btnConectar = new JButton("Conectar");
        btnConectar.setBounds(231 , 190, 89, 23);
        panel.add(btnConectar);
        
        errorLabel = new JLabel("");
        errorLabel.setVerticalAlignment(SwingConstants.TOP);
        errorLabel.setFont(new Font("Lucida Grande", Font.PLAIN, 12));
        errorLabel.setBounds(10, 165, 310, 43);
        panel.add(errorLabel);

    }

    public void show() {
        frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        frame.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                int confirm = JOptionPane.showOptionDialog(
                        null, "¿Estás seguro que quieres salir de la Agenda?",
                        "Confirmación", JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE, null, null, null);
                if (confirm == 0) {
                    try {
                        Conexion.getConexion().cerrarConexion();
                        System.exit(0);
                    }
                    catch (Exception err) {
                        System.exit(0);
                    }
                }
            }
        });
        frame.setVisible(true);
    }
    
    public void close() {
    	frame.dispose();
    }

    public void editarConexion() {
        btnConectar.setVisible(false);
    }

    public JTextField getTxtDbIp() {
        return txtDbIp;
    }

    public JTextField getTxtDbPassword() {
        return txtDbPassword;
    }

    public JTextField getTxtDbUser() {
        return txtDbUser;
    }

    public JButton getBtnConectar() {
        return btnConectar;
    }

    public JButton getBtnGuardar() {
        return btnGuardar;
    }

	public JLabel getErrorLabel() {
		return errorLabel;
	}

	public JTextField getTxtPort() {
        return txtPort;
    }
}

package presentacion.vista;

import dto.LocalidadDTO;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class VentanaLocalidad extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JButton btnAgregar;
	private JButton btnEditar;
	private JButton btnSalir;
	private JTextField txtNombre;
	private JTextField txtId;
	private static VentanaLocalidad INSTANCE;

	public static VentanaLocalidad getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new VentanaLocalidad();
			return new VentanaLocalidad();
		} else {
			return INSTANCE;
		}
	}

	private VentanaLocalidad() {
		super();

		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 400, 250);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JPanel panel = new JPanel();
		panel.setBounds(10, 11, 440, 307);
		contentPane.add(panel);
		panel.setLayout(null);

		JLabel lblNombreYApellido = new JLabel("Nombre de Localidad:");
		lblNombreYApellido.setBounds(10, 11, 152, 16);
		panel.add(lblNombreYApellido);

		txtNombre = new JTextField(5);
		txtNombre.setBounds(200, 8, 160, 20);
		txtNombre.setToolTipText("Enter your username over here, that other thing is a label.");
		txtNombre.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (txtNombre.getText().length() >= 45) {
					JOptionPane.showMessageDialog(null,
							"El nombre de la localidad no puede superar los 45 caracteres.");
					e.consume();
				}
			}
		});
		panel.add(txtNombre);
		txtNombre.setColumns(10);

		btnAgregar = new JButton("Agregar");
		btnAgregar.setBounds(190, 185, 89, 23);
		panel.add(btnAgregar);

		btnEditar = new JButton("Guardar");
		btnEditar.setBounds(190, 185, 89, 23);
		panel.add(btnEditar);

		btnSalir = new JButton("Salir");
		btnSalir.setBounds(290, 185, 89, 23);
		btnSalir.addActionListener(this::cerrarVentana);
		panel.add(btnSalir);

		txtId = new JTextField();
		panel.add(txtId);
		txtId.setVisible(false);

	}

	private void cerrarVentana(ActionEvent actionEvent) {
		this.dispose();
	}

	public void editarLocalidad(LocalidadDTO localidad) {
		btnAgregar.setVisible(false);
		btnEditar.setVisible(true);
		txtNombre.setText(localidad.getNombre());
		txtId.setText(String.valueOf(localidad.getId()));
		this.setVisible(true);
	}

	public void crearLocalidad() {
		btnAgregar.setVisible(true);
		btnEditar.setVisible(false);
		txtNombre.setText(null);
		txtId.setText(null);
		this.setVisible(true);
	}

	public JButton getBtnAgregar() {
		return btnAgregar;
	}

	public JButton getBtnEditar() {
		return btnEditar;
	}

	public JTextField getTxtNombre() {
		return txtNombre;
	}

	public JTextField getTxtId() {
		return txtId;
	}

	public void cerrar() {
		this.txtNombre.setText(null);
		this.dispose();
	}
}

package presentacion.vista;

import dto.LocalidadDTO;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.util.List;

public class VistaLocalidad extends JFrame {
    private JFrame frame;
    private JTable tablaLocalidades;
    private JButton btnAgregarLocalidad;
    private JButton btnEditarLocalidad;
    private JButton btnBorrarLocalidad;
    private JButton btnSalir;
   
    private DefaultTableModel modelLocalidades;
    private static VistaLocalidad INSTANCE;
    private  String[] nombreColumnas = {"id","Nombre"};

    private VistaLocalidad() {
        super();
        initialize();
    }

    public static VistaLocalidad getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new VistaLocalidad();
            return new VistaLocalidad();
        }
        else {
            return INSTANCE;
        }
    }

    private void initialize() {
        frame = new JFrame();
        frame.setBounds(100, 100, 410, 300);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        frame.getContentPane().setLayout(null);

        JPanel panel = new JPanel();
        panel.setBounds(0, 0, 410, 290);
        frame.getContentPane().add(panel);
        panel.setLayout(null);

        modelLocalidades = new DefaultTableModel(null,nombreColumnas);
        tablaLocalidades = new JTable(modelLocalidades);

        JScrollPane spEtiquetas = new JScrollPane();
        spEtiquetas.setBounds(10, 10, 390, 190);
        panel.add(spEtiquetas);

        tablaLocalidades.getColumnModel().getColumn(0).setPreferredWidth(103);
        tablaLocalidades.getColumnModel().getColumn(0).setResizable(false);
        tablaLocalidades.getColumnModel().getColumn(1).setPreferredWidth(100);
        tablaLocalidades.getColumnModel().getColumn(1).setResizable(false);

        spEtiquetas.setViewportView(tablaLocalidades);

        btnEditarLocalidad = new JButton("Editar");
        btnEditarLocalidad.setBounds(109, 240, 89, 23);
        panel.add(btnEditarLocalidad);

        btnBorrarLocalidad = new JButton("Borrar");
        btnBorrarLocalidad.setBounds(208, 240, 89, 23);
        panel.add(btnBorrarLocalidad);

        btnAgregarLocalidad = new JButton("Agregar");
        btnAgregarLocalidad.setBounds(10, 240, 89, 23);
        panel.add(btnAgregarLocalidad);

        btnSalir = new JButton("Salir");
        btnSalir.setBounds(310, 240, 89, 23);
        panel.add(btnSalir);
    }
    
    public JButton getBtnSalir() {
    	return btnSalir;
    }
    
    public JFrame getFrame() {
    	return frame;
    }
    
    public JButton getBtnAgregarLocalidad() {
        return btnAgregarLocalidad;
    }

    public JButton getBtnEditarLocalidad() {
        return btnEditarLocalidad;
    }

    public JButton getBtnBorrarLocalidad() {
        return btnBorrarLocalidad;
    }

    public boolean confirmarBorrado() {
        return(JOptionPane.showOptionDialog(null,
                "¿Estás seguro que quieres borrar las etiquetas selecionadas?",
                "Confirmación",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                null,
                null) == 0);
    }

    public void mostrarAdvertencia(String advertencia) {
        JOptionPane.showMessageDialog(null, advertencia);
    }

    public void show() {
        this.frame.setVisible(true);
    }

    public String[] getNombreColumnas() {
        return nombreColumnas;
    }

    public DefaultTableModel getModelLocalidades() {
        return modelLocalidades;
    }

    public JTable getTablaLocalidad() {
        return tablaLocalidades;
    }

    public void llenarTabla(List<LocalidadDTO> localidades) {
        this.modelLocalidades.setRowCount(0); //Para vaciar la tabla
        this.modelLocalidades.setColumnCount(0);
        this.modelLocalidades.setColumnIdentifiers(this.getNombreColumnas());

        for (LocalidadDTO p : localidades) {
            String nombre = p.getNombre();
            int id = p.getId();
            Object[] fila = {id,nombre};
            this.modelLocalidades.addRow(fila);
        }

    }
}

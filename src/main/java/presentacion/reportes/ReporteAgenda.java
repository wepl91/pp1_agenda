package presentacion.reportes;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;
import dto.EtiquetaDTO;
import dto.LocalidadDTO;
import dto.PersonaDTO;
import dto.PersonaReportDTO;

public class ReporteAgenda
{
	private JasperReport reporte;
	private JasperViewer reporteViewer;
	private JasperPrint	reporteLleno;
	private Logger log = Logger.getLogger(ReporteAgenda.class);
	//Recibe la lista de personas para armar el reporte
    public ReporteAgenda(List<PersonaDTO> personas,List<EtiquetaDTO> etiquetasEnTabla,List<LocalidadDTO> localidadesEnTabla)
    {
    	
    	//Completo el DTO especifico del jasper
    	List<PersonaReportDTO> personasReport = new ArrayList<>();
    	for(PersonaDTO persona:personas) {
    		//Completo el DTO especifico del reporte
    		PersonaReportDTO personaReportDTO=new PersonaReportDTO(persona);
    		//busco la localidad
    		for(LocalidadDTO localidadDTO: localidadesEnTabla) {
    			if(localidadDTO.getId()==persona.getIdLocalidad()) {
    				personaReportDTO.setLocalidad(localidadDTO.getNombre());
    				break;
    			}
    		}
    		//Busco la etiqueta	
    		for(EtiquetaDTO etiquetaDTO: etiquetasEnTabla) {
    			if(etiquetaDTO.getId()==persona.getIdEtiqueta()) {
    				personaReportDTO.setEtiqueta(etiquetaDTO.getNombre());
    				break;
    			}
    		}
    		personasReport.add(personaReportDTO);
    	}
    
    	//Hardcodeado
		Map<String, Object> parametersMap = new HashMap<String, Object>();
		parametersMap.put("Fecha", new SimpleDateFormat("dd/MM/yyyy").format(new Date()));
		parametersMap.put("totalPersonas",personasReport.size() );	
    	try		{
			this.reporte = (JasperReport) JRLoader.loadObjectFromFile( "reportes" + File.separator + "ReporteAgenda_ent2.jasper" );
			this.reporteLleno = JasperFillManager.fillReport(this.reporte, parametersMap, 
					new JRBeanCollectionDataSource(personasReport));
    		log.info("Se cargó correctamente el reporte");
		}
		catch( JRException ex ) 
		{
			log.error("Ocurrió un error mientras se cargaba el archivo ReporteAgenda.Jasper", ex);
		}
    }       
    
    public void mostrar()
	{
		this.reporteViewer = new JasperViewer(this.reporteLleno,false);
		this.reporteViewer.setVisible(true);
	}
   
}	
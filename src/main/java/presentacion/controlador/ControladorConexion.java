package presentacion.controlador;

import presentacion.vista.VentanaDB;
import presentacion.vista.VistaPersona;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Properties;
import persistencia.conexion.Conexion;

public class ControladorConexion implements ActionListener {
    
	private boolean fromVentanaPersonas;
	private VentanaDB ventanaDB;
    private VistaPersona vistaPersona;
    private Properties prop;
    private ControladorPersona controladorPersona;
	private ControladorEtiqueta controladorEtiqueta;
	private ControladorLocalidad controladorLocalidad;
    

    public ControladorConexion(VistaPersona vistaPersona,
    		ControladorPersona controladorPersona,
    		ControladorEtiqueta controladorEtiqueta,
    		ControladorLocalidad controladorLocalidad) {
    	
    	this.vistaPersona = vistaPersona;
    	this.controladorPersona=controladorPersona;
    	this.controladorEtiqueta=controladorEtiqueta;
    	this.controladorLocalidad=controladorLocalidad;
        
    	ventanaDB = VentanaDB.getInstance();
        this.vistaPersona.getBtnEditarConexion().addActionListener(this::editarConexion);
        this.ventanaDB.getBtnGuardar().addActionListener(this::guardarDatosConexion);
        this.ventanaDB.getBtnConectar().addActionListener(this::testConectionFromWindow);
        
    }
    
    public void inicializarApp() {
    	controladorPersona.inicializar();
		controladorEtiqueta.inicializar();
		controladorLocalidad.inicializar();
    }
    
    public void showVentanaDb() {
    	fromVentanaPersonas=false;
    	ventanaDB.getBtnGuardar().setVisible(false);
    	ventanaDB.getBtnConectar().setVisible(true);
    	ventanaDB.show();
    }

    private void editarConexion(ActionEvent a) {
    	fromVentanaPersonas=true;
    	ventanaDB.getBtnConectar().setVisible(false);
    	ventanaDB.getBtnGuardar().setVisible(true);
    	ventanaDB.show();
    }

    private void guardarDatosConexion(ActionEvent a) {
        String ip = this.ventanaDB.getTxtDbIp().getText();
        String port = this.ventanaDB.getTxtPort().getText();
        String user = this.ventanaDB.getTxtDbUser().getText();
        String password = this.ventanaDB.getTxtDbPassword().getText();

        try (OutputStream output = new FileOutputStream("agenda.properties")) {
        	prop.setProperty("user", user);
            prop.setProperty("ip", ip);
            prop.setProperty("port", port);
            prop.setProperty("password", password);
            prop.store(output, null);

        } catch (IOException io) {
            io.printStackTrace();
        }
        if(fromVentanaPersonas) {
        	ventanaDB.close();
        }
    }
    private void getProperties() {
		Properties prop = new Properties();
		try {
			prop.load(new FileInputStream("agenda.properties"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		this.prop=prop;
	}
    
    public boolean testConectionFromProperties() {
        
    	if(prop==null) {
    		getProperties();
    	}
    	
    	String ip = prop.getProperty("ip");
        String user = prop.getProperty("user");
        String password = prop.getProperty("password");
        String port = prop.getProperty("port");

        this.ventanaDB.getTxtDbIp().setText(ip);
        this.ventanaDB.getTxtPort().setText(port);
        this.ventanaDB.getTxtDbUser().setText(user);
        this.ventanaDB.getTxtDbPassword().setText(password);
        
        if(ip==null) {
        	return false;
        }
        return testConection(ip, port, user, password);
    }
    

    private void testConectionFromWindow(ActionEvent a) {
        String ip = this.ventanaDB.getTxtDbIp().getText();
        String port = this.ventanaDB.getTxtPort().getText();
        String user = this.ventanaDB.getTxtDbUser().getText();
        String password = this.ventanaDB.getTxtDbPassword().getText();

        if(testConection(ip, port, user, password)) {
            inicializarApp();
            ventanaDB.close();
        }
    }

	private boolean testConection(String ip, String port, String user, String password) {
		Conexion conn=Conexion.getConexion();
		conn.setIp(ip);
		conn.setPort(port);
		conn.setUser(user);
        conn.setPassword(password);
        
        if (Conexion.getConexion().getSQLConexion()!=null) {
            this.ventanaDB.getErrorLabel().setText("");
            return true;
        }
        this.ventanaDB.getErrorLabel().setText("Error en la conexión");
        return false;
	}

    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub
    }
}

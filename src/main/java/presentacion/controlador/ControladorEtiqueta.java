package presentacion.controlador;

import dto.EtiquetaDTO;
import dto.LocalidadDTO;
import dto.PersonaDTO;
import modelo.Agenda;
import presentacion.vista.VentanaEtiqueta;
import presentacion.vista.VentanaPersona;
import presentacion.vista.VistaPersona;
import presentacion.vista.VistaEtiqueta;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class ControladorEtiqueta implements ActionListener {
    private VistaPersona vistaPersona;
    private Agenda agenda;
    private List<EtiquetaDTO> etiquetasEnTabla;
    private VistaEtiqueta vistaEtiquetas;
    private VentanaEtiqueta ventanaEtiqueta;

    public ControladorEtiqueta(VistaPersona vistaPersona, Agenda agenda) {
        this.vistaPersona = vistaPersona;
        this.vistaPersona.getBtnEtiquetas().addActionListener(this::ventanaEtiquetas);

        this.vistaEtiquetas = VistaEtiqueta.getInstance();
        this.ventanaEtiqueta = VentanaEtiqueta.getInstance();

        this.vistaEtiquetas.getBtnAgregarEtiqueta().addActionListener(this::ventanaCrearEtiqueta);
        this.vistaEtiquetas.getBtnBorrarEtiqueta().addActionListener(this::borrarEtiqueta);
        this.vistaEtiquetas.getBtnEditarEtiqueta().addActionListener(this::ventanaEditarEtiqueta);
        this.vistaEtiquetas.getBtnSalir().addActionListener(this::cerrarVentanaEtiquetas);

        this.ventanaEtiqueta.getBtnAgregar().addActionListener(this::crearEtiqueta);
        this.ventanaEtiqueta.getBtnEditar().addActionListener(this::editarEtiqueta);

        this.agenda = agenda;
    }
    
    private void cerrarVentanaEtiquetas(ActionEvent actionEvent) {
    	this.refrescarTablaPersona();
    	this.vistaEtiquetas.getFrame().dispose();
    }
    
    private void refrescarTablaPersona() {
        List<PersonaDTO> personas = agenda.obtenerPersonas();
        List<EtiquetaDTO> etiquetas = agenda.obtenerEtiquetas();
        List<LocalidadDTO> localidades = agenda.obtenerLocalidades();
        this.vistaPersona.llenarTabla(personas, etiquetas, localidades);
    }

    private void ventanaEtiquetas(ActionEvent actionEvent) {
        this.vistaEtiquetas.show();
    }

    private void ventanaEditarEtiqueta(ActionEvent a) {
        int[] filasSeleccionadas = this.vistaEtiquetas.getTablaEtiquetas().getSelectedRows();
        if (filasSeleccionadas.length > 1) {
            this.vistaPersona.mostrarAdvertencia("No se puede editar más de una etiqueta a la vez.");
        } else if (filasSeleccionadas.length == 0) {
            this.vistaPersona.mostrarAdvertencia("No se ha seleccionado ninguna etiqueta para editar.");
        } else {
            this.ventanaEtiqueta.editarEtiqueta(this.etiquetasEnTabla.get(filasSeleccionadas[0]));
        }
    }

    private void ventanaCrearEtiqueta(ActionEvent a) {
        this.ventanaEtiqueta.crearEtiqueta();
    }

    private void editarEtiqueta(ActionEvent a) {
        EtiquetaDTO etiqueta = this.agenda.buscarEtiqueta(Integer.parseInt(this.ventanaEtiqueta.getTxtId().getText()));
        etiqueta.setNombre(this.ventanaEtiqueta.getTxtNombre().getText());

        this.agenda.actualizarEtiqueta(etiqueta);
        this.refrescarEtiquetas();
        this.ventanaEtiqueta.cerrar();
    }

    private void borrarEtiqueta(ActionEvent s) {
        int[] filasSeleccionadas = this.vistaEtiquetas.getTablaEtiquetas().getSelectedRows();
        if (filasSeleccionadas.length < 1) {
            this.vistaPersona.mostrarAdvertencia("No seleccionaste ninguna etiqueta para borrar.");
        } else {
            if (this.vistaPersona.confirmarBorrado("etiquetas")) {
                for (int fila : filasSeleccionadas) {
                    if (!this.agenda.borrarEtiqueta(this.etiquetasEnTabla.get(fila))){
                        this.vistaPersona.mostrarAdvertencia("No es posible borrar la etiqueta debido a que se encuentra asignada a un contacto de la agenda.");
                    }

                }
                this.refrescarEtiquetas();
            }
        }
    }

    private void crearEtiqueta(ActionEvent p) {
        String nombreEtiqueta = this.ventanaEtiqueta.getTxtNombre().getText();
        EtiquetaDTO nuevaEtiqueta = new EtiquetaDTO(0, nombreEtiqueta);

        this.agenda.agregarEtiqueta(nuevaEtiqueta);
        this.refrescarEtiquetas();
        this.ventanaEtiqueta.cerrar();
    }

    private void refrescarEtiquetas() {
        this.etiquetasEnTabla = this.agenda.obtenerEtiquetas();
        this.vistaEtiquetas.llenarTabla(this.etiquetasEnTabla);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub
    }

    public void inicializar() {
        this.refrescarEtiquetas();
    }
}

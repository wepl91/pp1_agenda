package presentacion.controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import dto.EtiquetaDTO;
import dto.LocalidadDTO;
import modelo.Agenda;
import presentacion.reportes.ReporteAgenda;
import presentacion.vista.VentanaPersona;
import presentacion.vista.VistaPersona;
import dto.PersonaDTO;

public class ControladorPersona implements ActionListener {
    private VistaPersona vistaPersona;
    private List<PersonaDTO> personasEnTabla;
    private VentanaPersona ventanaPersona;
    private Agenda agenda;

    public ControladorPersona(VistaPersona vistaPersona, Agenda agenda) {
        this.vistaPersona = vistaPersona;
        this.vistaPersona.getBtnAgregar().addActionListener(this::ventanaAgregarPersona);
        this.vistaPersona.getBtnEditar().addActionListener(this::ventanaEditarPersona);
        this.vistaPersona.getBtnBorrar().addActionListener(this::borrarPersona);
        this.vistaPersona.getBtnReporte().addActionListener(this::mostrarReporte);

        this.ventanaPersona = VentanaPersona.getInstance();
        this.ventanaPersona.getBtnAgregarPersona().addActionListener(this::crearPersona);
        this.ventanaPersona.getBtnEditarPersona().addActionListener(this::editarPersona);

        this.agenda = agenda;
    }

    private void ventanaAgregarPersona(ActionEvent a) {
        this.ventanaPersona.llenarComboEtiqueta(this.agenda.obtenerEtiquetas());
        this.ventanaPersona.llenarComboLocalidad(this.agenda.obtenerLocalidades());
        this.ventanaPersona.crearPersona();
    }

    private void ventanaEditarPersona(ActionEvent a) {
        this.ventanaPersona.llenarComboEtiqueta(this.agenda.obtenerEtiquetas());
        this.ventanaPersona.llenarComboLocalidad(this.agenda.obtenerLocalidades());
        
        int[] filasSeleccionadas = this.vistaPersona.getTablaPersonas().getSelectedRows();
        if (filasSeleccionadas.length > 1) {
            this.vistaPersona.mostrarAdvertencia("No se puede editar más de una persona a la vez.");
        } else if (filasSeleccionadas.length == 0) {
            this.vistaPersona.mostrarAdvertencia("No se ha seleccionado ninguna persona para editar.");
        } else {
        	PersonaDTO persona = this.personasEnTabla.get((filasSeleccionadas[0]));
        	EtiquetaDTO etiqueta = this.agenda.buscarEtiqueta(persona.getIdEtiqueta());
        	LocalidadDTO localidad = this.agenda.buscarLocalidad(persona.getIdLocalidad());
            this.ventanaPersona.editarPersona(persona, etiqueta, localidad);
        }
    }

    private void crearPersona(ActionEvent p) {
        String altura = this.ventanaPersona.getTxtAltura().getText();
        String piso = this.ventanaPersona.getTxtPiso().getText();
        String email = this.ventanaPersona.getTxtEmail().getText();

        if (!validateEmail(email)) {
            this.vistaPersona.mostrarAdvertencia("Se ha ingresado un email con formato inválido.");
        } else if (!altura.equals("") && !altura.matches("[0-9]+")) {
            this.vistaPersona.mostrarAdvertencia("El campo de altura debe ser únicamente numérico.");
        } else if (!piso.equals("") && !piso.matches("[0-9]+")) {
            this.vistaPersona.mostrarAdvertencia("El campo de piso debe ser únicamente numérico.");
        } else {
            String nombre = this.ventanaPersona.getTxtNombre().getText();
            String tel = this.ventanaPersona.getTxtTelefono().getText();
            Date nacimiento=null;

            if (this.ventanaPersona.getNacimiento().isEnabled()) {
                nacimiento = (Date) this.ventanaPersona.getNacimiento().getModel().getValue();
            }

            PersonaDTO nuevaPersona = new PersonaDTO(0, nombre, tel, email, nacimiento);
            nuevaPersona.setCalle(this.ventanaPersona.getTxtCalle().getText());
            nuevaPersona.setDept(this.ventanaPersona.getTxtDepartamento().getText());
            nuevaPersona.setPiso(!piso.equals("") ? Integer.parseInt(piso) : null);
            nuevaPersona.setAltura(!altura.equals("") ? Integer.parseInt(altura) : null);

            EtiquetaDTO etiqueta = (EtiquetaDTO) this.ventanaPersona.getListEtiquetas().getSelectedItem();
            nuevaPersona.setIdEtiqueta(etiqueta.getId());
            LocalidadDTO localidad = (LocalidadDTO) this.ventanaPersona.getListLocalidad().getSelectedItem();
            nuevaPersona.setIdLocalidad(localidad.getId());

            this.agenda.agregarPersona(nuevaPersona);
            this.refrescarTabla();
            this.ventanaPersona.cerrar();
        }
    }

    private void editarPersona(ActionEvent a) {
        String altura = this.ventanaPersona.getTxtAltura().getText();
        String piso = this.ventanaPersona.getTxtPiso().getText();
        String email = this.ventanaPersona.getTxtEmail().getText();

        if (!validateEmail(email)) {
            this.vistaPersona.mostrarAdvertencia("Se ha ingresado un email con formato inválido.");
        }
        else if (!altura.equals("") && !altura.matches("[0-9]+")) {
            this.vistaPersona.mostrarAdvertencia("El campo de altura debe ser únicamente numérico.");
        }
        else if (!piso.equals("") && !piso.matches("[0-9]+")) {
            this.vistaPersona.mostrarAdvertencia("El campo de piso debe ser únicamente numérico.");
        }
        else {
            PersonaDTO persona = this.agenda.buscarPersona(Integer.parseInt(this.ventanaPersona.getTxtId().getText()));
            if (this.ventanaPersona.getNacimiento().isEnabled()) {
                Date nacimientoPersona = (Date) this.ventanaPersona.getNacimiento().getModel().getValue();
                persona.setNacimiento(new java.sql.Date(nacimientoPersona.getTime()));
            }
            else {
                persona.setNacimiento(null);
            }

            EtiquetaDTO etiqueta = (EtiquetaDTO) this.ventanaPersona.getListEtiquetas().getSelectedItem();
            persona.setIdEtiqueta(etiqueta.getId());
            LocalidadDTO localidad = (LocalidadDTO) this.ventanaPersona.getListLocalidad().getSelectedItem();
            persona.setIdLocalidad(localidad.getId());

            persona.setNombre(this.ventanaPersona.getTxtNombre().getText());
            persona.setTelefono(this.ventanaPersona.getTxtTelefono().getText());
            persona.setEmail(email);
            persona.setCalle(this.ventanaPersona.getTxtCalle().getText());
            persona.setDept(this.ventanaPersona.getTxtDepartamento().getText());
            persona.setPiso(!piso.equals("") ? Integer.parseInt(piso) : null);
            persona.setAltura(!altura.equals("") ? Integer.parseInt(altura) : null);
            
            EtiquetaDTO etiquetaDTO=(EtiquetaDTO) this.ventanaPersona.getListEtiquetas().getSelectedItem();
            persona.setIdEtiqueta(etiquetaDTO.getId()); 
            LocalidadDTO localidadDTO=(LocalidadDTO) this.ventanaPersona.getListLocalidad().getSelectedItem();
            persona.setIdLocalidad(localidadDTO.getId());
            
            this.agenda.actualizarPersona(persona);
            this.refrescarTabla();
            this.ventanaPersona.cerrar();
        }
    }

    private void mostrarReporte(ActionEvent r) {
        ReporteAgenda reporte = new ReporteAgenda(agenda.obtenerPersonas(),this.agenda.obtenerEtiquetas(),this.agenda.obtenerLocalidades());
        reporte.mostrar();
    }

    private void borrarPersona(ActionEvent s) {
        int[] filasSeleccionadas = this.vistaPersona.getTablaPersonas().getSelectedRows();
        if (filasSeleccionadas.length < 1) {
            this.vistaPersona.mostrarAdvertencia("No seleccionaste ninguna persona para borrar.");
        } else {
            if (this.vistaPersona.confirmarBorrado("personas")) {
                for (int fila : filasSeleccionadas) {
                    this.agenda.borrarPersona(this.personasEnTabla.get(fila));
                }
                this.refrescarTabla();
            }
        }
    }

    public void inicializar() {
        this.vistaPersona.show();
        this.refrescarTabla();
    }

    private void refrescarTabla() {
        this.personasEnTabla = agenda.obtenerPersonas();
        List<EtiquetaDTO> etiquetas = agenda.obtenerEtiquetas();
        List<LocalidadDTO> localidades = agenda.obtenerLocalidades();
        this.vistaPersona.llenarTabla(this.personasEnTabla, etiquetas, localidades);
    }

    private boolean validateEmail(String email) {
        return Pattern.matches("[_a-zA-Z1-9]+(\\.[A-Za-z0-9]*)*@[A-Za-z0-9]+\\.[A-Za-z0-9]+(\\.[A-Za-z0-9]*)*", email);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub
    }
}

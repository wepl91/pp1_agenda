package presentacion.controlador;

import dto.EtiquetaDTO;
import dto.LocalidadDTO;
import dto.PersonaDTO;
import modelo.Agenda;
import presentacion.vista.VentanaLocalidad;
import presentacion.vista.VentanaPersona;
import presentacion.vista.VistaLocalidad;
import presentacion.vista.VistaPersona;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

public class ControladorLocalidad implements ActionListener {
    private VistaPersona vistaPersona;
    private Agenda agenda;
    private VentanaLocalidad ventanaLocalidad;
    private VistaLocalidad vistaLocalidad;
    private List<LocalidadDTO> localidadesEnTabla;

    public ControladorLocalidad(VistaPersona vistaPersona, Agenda agenda) {
        this.vistaPersona = vistaPersona;
        this.vistaPersona.getBtnLocalidades().addActionListener(this::ventanaLocalidades);

        this.ventanaLocalidad = VentanaLocalidad.getInstance();
        this.vistaLocalidad = VistaLocalidad.getInstance();

        this.vistaLocalidad.getBtnAgregarLocalidad().addActionListener(this::ventanaCrearLocalidad);
        this.vistaLocalidad.getBtnBorrarLocalidad().addActionListener(this::borrarLocalidad);
        this.vistaLocalidad.getBtnEditarLocalidad().addActionListener(this::ventanaEditarLocalidad);
        this.vistaLocalidad.getBtnSalir().addActionListener(this::cerrarVentanaLocalidad);

        this.ventanaLocalidad.getBtnAgregar().addActionListener(this::crearLocalidad);
        this.ventanaLocalidad.getBtnEditar().addActionListener(this::editarLocalidad);

        this.agenda = agenda;
    }
    
    private void cerrarVentanaLocalidad(ActionEvent actionEvent) {
    	this.refrescarTablaPersona();
    	this.vistaLocalidad.getFrame().dispose();
    }
    
    private void refrescarTablaPersona() {
        List<PersonaDTO> personas = agenda.obtenerPersonas();
        List<EtiquetaDTO> etiquetas = agenda.obtenerEtiquetas();
        List<LocalidadDTO> localidades = agenda.obtenerLocalidades();
        this.vistaPersona.llenarTabla(personas, etiquetas, localidades);
    }

    private void ventanaLocalidades(ActionEvent actionEvent) {
        this.vistaLocalidad.show();
    }

    private void ventanaCrearLocalidad(ActionEvent a) {
        ventanaLocalidad.crearLocalidad();
    }

    private void crearLocalidad(ActionEvent p) {
        String nombre = this.ventanaLocalidad.getTxtNombre().getText();
        LocalidadDTO localidad = new LocalidadDTO(0, nombre);

        this.agenda.agregarLocalidad(localidad);
        this.refrescarLocalidades();
        this.ventanaLocalidad.cerrar();
    }

    private void ventanaEditarLocalidad(ActionEvent a) {
        int[] filasSeleccionadas = this.vistaLocalidad.getTablaLocalidad().getSelectedRows();
        if (filasSeleccionadas.length > 1) {
            this.vistaPersona.mostrarAdvertencia("No se puede editar más de una localidad a la vez.");
        } else {
            this.ventanaLocalidad.editarLocalidad(this.localidadesEnTabla.get(filasSeleccionadas[0]));
        }
    }

    private void refrescarLocalidades() {
        this.localidadesEnTabla = agenda.obtenerLocalidades();
        this.vistaLocalidad.llenarTabla(this.localidadesEnTabla);
    }

    private void borrarLocalidad(ActionEvent s) {
        int[] filasSeleccionadas = this.vistaLocalidad.getTablaLocalidad().getSelectedRows();
        if (filasSeleccionadas.length < 1) {
            this.vistaPersona.mostrarAdvertencia("No seleccionaste ninguna localidad para borrar.");
        } else {
            if (this.vistaPersona.confirmarBorrado("localidades")) {
                for (int fila : filasSeleccionadas) {
                    if(!this.agenda.borrarLocalidad(this.localidadesEnTabla.get(fila))) {
                        this.vistaPersona.mostrarAdvertencia("No es posible borrar la localidad debido a que se encuentra asignada a un contacto de la agenda.");
                    }
                }
                this.refrescarLocalidades();
            }
        }
    }

    private void editarLocalidad(ActionEvent a) {
        LocalidadDTO localidad = this.agenda.buscarLocalidad(Integer.parseInt(this.ventanaLocalidad.getTxtId().getText()));
        localidad.setNombre(this.ventanaLocalidad.getTxtNombre().getText());

        this.agenda.actualizarLocalidad(localidad);
        this.refrescarLocalidades();
        this.ventanaLocalidad.cerrar();
    }

    private void mostrarEditarLocalidades(ActionEvent r) {
        vistaLocalidad.show();
    }

    public void inicializar() {
        this.refrescarLocalidades();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // TODO Auto-generated method stub
    }
}

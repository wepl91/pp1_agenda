package persistencia.dao.interfaz;

import java.util.List;

import dto.PersonaDTO;

public interface PersonaDAO {
	
	public boolean insert(PersonaDTO persona);

	public boolean delete(PersonaDTO persona_a_eliminar);

	public boolean update(PersonaDTO persona_a_editar);

	public PersonaDTO find(int personaId_a_buscar);
	
	public List<PersonaDTO> readAll();
}

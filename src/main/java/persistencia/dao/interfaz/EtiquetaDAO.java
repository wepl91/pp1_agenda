package persistencia.dao.interfaz;

import java.util.List;

import dto.EtiquetaDTO;

public interface EtiquetaDAO {

    public List<EtiquetaDTO> readAll();

    public boolean insert(EtiquetaDTO etiqueta);

    public boolean delete(EtiquetaDTO etiqueta_a_eliminar);

    public boolean update(EtiquetaDTO etiqueta_a_actualiza);

    public EtiquetaDTO find(int eiquetaId_a_buscar);
}

package persistencia.dao.mysql;

import persistencia.dao.interfaz.EtiquetaDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.EtiquetaDAO;
import dto.EtiquetaDTO;

public class EtiquetaDAOSQL implements EtiquetaDAO {
    private static final String insert = "INSERT INTO etiquetas(id, nombre) VALUES(?, ?)";
    private static final String readall = "SELECT * FROM etiquetas";
    private static final String delete = "DELETE FROM etiquetas WHERE id = ?";
    private static final String update = "UPDATE etiquetas SET nombre = ? where id = ?";

    public boolean insert(EtiquetaDTO etiqueta) {
        PreparedStatement statement;
        Connection conexion = Conexion.getConexion().getSQLConexion();
        boolean isInsertExitoso = false;
        try {
            statement = conexion.prepareStatement(insert);
            statement.setInt(1, etiqueta.getId());
            statement.setString(2, etiqueta.getNombre());
            if(statement.executeUpdate() > 0) {
                conexion.commit();
                isInsertExitoso = true;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
            try {
                conexion.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }

        return isInsertExitoso;
    }

    public List<EtiquetaDTO> readAll() {
        PreparedStatement statement;
        ResultSet resultSet; //Guarda el resultado de la query
        ArrayList<EtiquetaDTO> personas = new ArrayList<EtiquetaDTO>();
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(readall);
            resultSet = statement.executeQuery();
            while(resultSet.next()) {
                personas.add(getEtiquetaDto(resultSet));
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return personas;
    }

    public boolean delete(EtiquetaDTO etiqueta_a_eliminar) {
        PreparedStatement statement;
        Connection conexion = Conexion.getConexion().getSQLConexion();
        boolean isdeleteExitoso = false;
        try {
            statement = conexion.prepareStatement(delete);
            statement.setString(1, Integer.toString(etiqueta_a_eliminar.getId()));
            if(statement.executeUpdate() > 0)
            {
                conexion.commit();
                isdeleteExitoso = true;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return isdeleteExitoso;
    }

    public EtiquetaDTO find(int id) {
        PreparedStatement statement;
        Conexion conexion = Conexion.getConexion();
        ResultSet resultSet;
        ArrayList<EtiquetaDTO> etiquetas = new ArrayList<EtiquetaDTO>();
        try {
            statement = conexion.getSQLConexion().prepareStatement("SELECT * FROM etiquetas WHERE id = ?");
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            while(resultSet.next()) {
                etiquetas.add(getEtiquetaDto(resultSet));
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }

        return etiquetas.get(0);
    }

    public boolean update(EtiquetaDTO etiqueta) {
        PreparedStatement statement;
        Connection conexion = Conexion.getConexion().getSQLConexion();
        boolean isUpdateExitoso = false;
        try {
            statement = conexion.prepareStatement(update);
            statement.setString(1, etiqueta.getNombre());
            statement.setInt(2, etiqueta.getId());
            if(statement.executeUpdate() > 0) {
                conexion.commit();
                isUpdateExitoso = true;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
            try {
                conexion.rollback();
            }
            catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
        return isUpdateExitoso;
    }

    private EtiquetaDTO getEtiquetaDto(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String nombre = resultSet.getString("Nombre");

        return new EtiquetaDTO(id, nombre);
    }
}

package persistencia.dao.mysql;

import persistencia.dao.interfaz.LocalidadDAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jfree.util.Log;

import persistencia.conexion.Conexion;
import dto.LocalidadDTO;

public class LocalidadDAOSQL implements LocalidadDAO {
    private static final String insert = "INSERT INTO localidades (id, nombre) VALUES(?, ?)";
    private static final String readall = "SELECT * FROM localidades";
    private static final String delete = "DELETE FROM localidades WHERE id = ?";
    private static final String update = "UPDATE localidades SET nombre = ? where id = ?";
    private static final String read = "SELECT * FROM localidades WHERE id = ?";

    public boolean insert(LocalidadDTO localidad) {
        PreparedStatement statement;
        Connection conexion = Conexion.getConexion().getSQLConexion();
        boolean isInsertExitoso = false;
        try {
            statement = conexion.prepareStatement(insert);
            statement.setInt(1, localidad.getId());
            statement.setString(2, localidad.getNombre());
            if(statement.executeUpdate() > 0) {
                conexion.commit();
                isInsertExitoso = true;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
            try {
                conexion.rollback();
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
        }

        return isInsertExitoso;
    }

    public List<LocalidadDTO> readAll() {
        PreparedStatement statement;
        ResultSet resultSet; //Guarda el resultado de la query
        ArrayList<LocalidadDTO> localidades = new ArrayList<LocalidadDTO>();
        Conexion conexion = Conexion.getConexion();
        try {
            statement = conexion.getSQLConexion().prepareStatement(readall);
            resultSet = statement.executeQuery();
            while(resultSet.next()) {
            	localidades.add(getLocalidadDto(resultSet));
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return localidades;
    }

    public boolean delete(LocalidadDTO localidad) {
        PreparedStatement statement;
        Connection conexion = Conexion.getConexion().getSQLConexion();
        boolean isdeleteExitoso = false;
        try {
            statement = conexion.prepareStatement(delete);
            statement.setInt(1, localidad.getId());
            if(statement.executeUpdate() > 0)
            {
                conexion.commit();
                isdeleteExitoso = true;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return isdeleteExitoso;
    }

    public LocalidadDTO find(int id) {
        PreparedStatement statement;
        Conexion conexion = Conexion.getConexion();
        ResultSet resultSet;
        LocalidadDTO localidad = null;
        try {
            statement = conexion.getSQLConexion().prepareStatement(read);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            resultSet.next();
            localidad = getLocalidadDto(resultSet);
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        return localidad;
    }

    public boolean update(LocalidadDTO localidad) {
        PreparedStatement statement;
        Connection conexion = Conexion.getConexion().getSQLConexion();
        boolean isUpdateExitoso = false;
        try {
            statement = conexion.prepareStatement(update);
            statement.setString(1, localidad.getNombre());
            statement.setInt(2, localidad.getId());
            if(statement.executeUpdate() > 0) {
                conexion.commit();
                isUpdateExitoso = true;
            }
        }
        catch (SQLException e) {
            e.printStackTrace();
            try {
                conexion.rollback();
            }
            catch (SQLException e1) {
                e1.printStackTrace();
            }
        }
        return isUpdateExitoso;
    }

    private LocalidadDTO getLocalidadDto(ResultSet resultSet) throws SQLException {
        int id = resultSet.getInt("id");
        String nombre = resultSet.getString("Nombre");

        return new LocalidadDTO(id, nombre);
    }
}

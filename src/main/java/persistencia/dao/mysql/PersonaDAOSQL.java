package persistencia.dao.mysql;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import persistencia.conexion.Conexion;
import persistencia.dao.interfaz.PersonaDAO;
import dto.PersonaDTO;

public class PersonaDAOSQL implements PersonaDAO {
	private static final String insert = "INSERT INTO personas(id,nombre, telefono, email, nacimiento,calle,altura,piso,dept,idetiqueta,idlocalidad) VALUES (?, ?, ?, ?, ?, ?,?,?,?,?,?)";
	private static final String delete = "DELETE FROM personas WHERE id = ?";
	private static final String update = "UPDATE personas SET nombre = ?, telefono = ?, email = ?, nacimiento = ?, idetiqueta = ?,calle = ?, altura = ?,piso = ?,dept = ?,idlocalidad = ? where id = ?";
	private static final String readall = "SELECT * FROM personas";
		
	public boolean insert(PersonaDTO persona) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isInsertExitoso = false;
		try {
			statement = conexion.prepareStatement(insert);
			statement.setInt(1, persona.getId());
			statement.setString(2, persona.getNombre());
			statement.setString(3, persona.getTelefono());
			statement.setString(4, persona.getEmail());
			statement.setDate(5, new java.sql.Date(persona.getNacimiento().getTime()));
			statement.setString(6, persona.getCalle());
			if (persona.getAltura() != null) {
				statement.setInt(7, persona.getAltura());
			}
			else {
				statement.setNull(7,0);
			}
			if (persona.getPiso() != null) {
				statement.setInt(8, persona.getPiso());
			}
			else {
				statement.setNull(8,0);
			}
			statement.setString(9, persona.getDept());	
			statement.setInt(10, persona.getIdEtiqueta());
			statement.setInt(11, persona.getIdLocalidad());

			if(statement.executeUpdate() > 0) {
				conexion.commit();
				isInsertExitoso = true;
			}
		} 
		catch (SQLException e) {
			e.printStackTrace();
			try {
				conexion.rollback();
			} catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		
		return isInsertExitoso;
	}

	public boolean update(PersonaDTO persona) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isUpdateExitoso = false;
		try {
			statement = conexion.prepareStatement(update);
			statement.setString(1, persona.getNombre());
			statement.setString(2, persona.getTelefono());
			statement.setString(3, persona.getEmail());
			statement.setDate(4, (Date) persona.getNacimiento());
			statement.setString(6, persona.getCalle());
			statement.setString(9, persona.getDept());
			statement.setInt(5, persona.getIdEtiqueta());
			statement.setInt(10, persona.getIdLocalidad());

			if (persona.getAltura() != null) {
				statement.setInt(7, persona.getAltura());
			}
			else {
				statement.setNull(7, java.sql.Types.INTEGER);
			}

			if (persona.getPiso() != null) {
				statement.setInt(8, persona.getPiso());
			}
			else {
				statement.setNull(8,java.sql.Types.INTEGER);
			}
			
			statement.setInt(11, persona.getId());
			
			if(statement.executeUpdate() > 0) {
				conexion.commit();
				isUpdateExitoso = true;
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
			try {
				conexion.rollback();
			}
			catch (SQLException e1) {
				e1.printStackTrace();
			}
		}
		return isUpdateExitoso;
	}

	public PersonaDTO find(int personaId) {
		PreparedStatement statement;
		Conexion conexion = Conexion.getConexion();
		ResultSet resultSet;
		ArrayList<PersonaDTO> personas = new ArrayList<PersonaDTO>();
		try {
			statement = conexion.getSQLConexion().prepareStatement("SELECT * FROM personas WHERE id = ?");
			statement.setInt(1, personaId);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				personas.add(getPersonaDTO(resultSet));
			}
		}
		catch (SQLException e) {
			e.printStackTrace();
		}

		return personas.get(0);
	}

	public boolean delete(PersonaDTO persona_a_eliminar) {
		PreparedStatement statement;
		Connection conexion = Conexion.getConexion().getSQLConexion();
		boolean isdeleteExitoso = false;
		try {
			statement = conexion.prepareStatement(delete);
			statement.setString(1, Integer.toString(persona_a_eliminar.getId()));
			if(statement.executeUpdate() > 0)
			{
				conexion.commit();
				isdeleteExitoso = true;
			}
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
		return isdeleteExitoso;
	}
	
	public List<PersonaDTO> readAll() {
		PreparedStatement statement;
		ResultSet resultSet; //Guarda el resultado de la query
		ArrayList<PersonaDTO> personas = new ArrayList<PersonaDTO>();
		Conexion conexion = Conexion.getConexion();
		try {
			statement = conexion.getSQLConexion().prepareStatement(readall);
			resultSet = statement.executeQuery();
			while(resultSet.next()) {
				personas.add(getPersonaDTO(resultSet));
			}
		} 
		catch (SQLException e) {
			e.printStackTrace();
		}
		return personas;
	}
	
	private PersonaDTO getPersonaDTO(ResultSet resultSet) throws SQLException {
		int id = resultSet.getInt("id");
		String nombre = resultSet.getString("Nombre");
		String tel = resultSet.getString("Telefono");
		String email = resultSet.getString("Email");
		Date nacimiento = resultSet.getDate("Nacimiento");
		
		PersonaDTO persona=new PersonaDTO(id, nombre, tel, email, nacimiento);
		persona.setCalle(resultSet.getString("calle"));
		persona.setAltura(resultSet.getInt("altura"));
		persona.setPiso(resultSet.getInt("piso"));
		persona.setDept(resultSet.getString("dept"));
		persona.setIdEtiqueta(resultSet.getInt("idEtiqueta"));
		persona.setIdLocalidad(resultSet.getInt("idLocalidad"));
		
		return persona;
	}
}

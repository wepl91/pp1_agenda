package dto;

import static org.junit.Assert.assertEquals;

import java.sql.Date;

import org.junit.Test;

public class TestPersonaReportDTO {

	@Test
	public void multiplicationOfZeroIntegersShouldReturnZero() {
		
		PersonaDTO personaDTO=new PersonaDTO(0,"bruno","2332","test@test.com",new Date(1974,0,1));
		PersonaReportDTO personaReportDTO = new PersonaReportDTO(personaDTO);
		assertEquals("Capricornio",personaReportDTO.getSigno()); 
		
		personaDTO=new PersonaDTO(0,"bruno","2332","test@test.com",new Date(1974,0,20));
		personaReportDTO = new PersonaReportDTO(personaDTO);
		assertEquals("Capricornio",personaReportDTO.getSigno()); 
		
		personaDTO=new PersonaDTO(0,"bruno","2332","test@test.com",new Date(1974,0,21));
		personaReportDTO = new PersonaReportDTO(personaDTO);
		assertEquals("Acuario",personaReportDTO.getSigno()); 
		
		personaDTO=new PersonaDTO(0,"bruno","2332","test@test.com",new Date(1974,1,2));
		personaReportDTO = new PersonaReportDTO(personaDTO);
		assertEquals("Acuario",personaReportDTO.getSigno());
		
		personaDTO=new PersonaDTO(0,"bruno","2332","test@test.com",new Date(1974,1,20));
		personaReportDTO = new PersonaReportDTO(personaDTO);
		assertEquals("Piscis",personaReportDTO.getSigno());
		
		personaDTO=new PersonaDTO(0,"bruno","2332","test@test.com",new Date(1974,2,18));
		personaReportDTO = new PersonaReportDTO(personaDTO);
		assertEquals("Piscis",personaReportDTO.getSigno());
		
		personaDTO=new PersonaDTO(0,"bruno","2332","test@test.com",new Date(1974,3,20));
		personaReportDTO = new PersonaReportDTO(personaDTO);
		assertEquals("Aries",personaReportDTO.getSigno());

		personaDTO=new PersonaDTO(0,"bruno","2332","test@test.com",new Date(1974,4,20));
		personaReportDTO = new PersonaReportDTO(personaDTO);
		assertEquals("Tauro",personaReportDTO.getSigno());
		
		personaDTO=new PersonaDTO(0,"bruno","2332","test@test.com",new Date(1974,5,20));
		personaReportDTO = new PersonaReportDTO(personaDTO);
		assertEquals("Géminis",personaReportDTO.getSigno());
		
		personaDTO=new PersonaDTO(0,"bruno","2332","test@test.com",new Date(1974,6,20));
		personaReportDTO = new PersonaReportDTO(personaDTO);
		assertEquals("Cáncer",personaReportDTO.getSigno());
		
		personaDTO=new PersonaDTO(0,"bruno","2332","test@test.com",new Date(1974,7,20));
		personaReportDTO = new PersonaReportDTO(personaDTO);
		assertEquals("Leo",personaReportDTO.getSigno());
		
		personaDTO=new PersonaDTO(0,"bruno","2332","test@test.com",new Date(1974,8,20));
		personaReportDTO = new PersonaReportDTO(personaDTO);
		assertEquals("Virgo",personaReportDTO.getSigno());
		
		personaDTO=new PersonaDTO(0,"bruno","2332","test@test.com",new Date(1974,9,20));
		personaReportDTO = new PersonaReportDTO(personaDTO);
		assertEquals("Libra",personaReportDTO.getSigno());
		
		personaDTO=new PersonaDTO(0,"bruno","2332","test@test.com",new Date(1974,10,20));
		personaReportDTO = new PersonaReportDTO(personaDTO);
		assertEquals("Escorpio",personaReportDTO.getSigno());
		
		personaDTO=new PersonaDTO(0,"bruno","2332","test@test.com",new Date(1974,11,20));
		personaReportDTO = new PersonaReportDTO(personaDTO);
		assertEquals("Sagitario",personaReportDTO.getSigno());
			
	}

	
}
